package com.nsw.parser.model;

import lombok.Data;

import javax.persistence.*;

@Data
@Entity(name = "base_user")
public class User {
    private static String BASE_JSON = "{\"applicationId\":null,\"userId\":\"416dacd5-840b-416d-a375-198238ec6bb2\",\"userName\":\":userName\",\"loweredUserName\":null,\"mobileAlias\":null,\"isAnonymous\":null,\"lastActivityDate\":null,\"password\":\":password\",\"passwordSalt\":\"yuwFaa8rwIu+Uxy8+SOfkQ==\",\"tabs\":[{\"tabId\":6,\"tabOrder\":16,\"tabName\":\"<img class=\\\"logobnn\\\" src=\\\"https://vnsw.gov.vn/homepage1/layout/emanifest/images/bocongthuong.png\\\">Trang chủ<a class=\\\"tenbo_tienganh\\\">Homepage</a>\",\"isVisible\":1,\"parentId\":null,\"level\":1,\"tabPath\":\"\",\"isDeleted\":0,\"menuType\":0,\"children\":null},{\"tabId\":162,\"tabOrder\":33,\"tabName\":\"<img class=\\\"logobnn\\\" src=\\\"https://vnsw.gov.vn/homepage1/layout/emanifest/images/bocongthuong.png\\\">Bộ Nông nghiệp<a class=\\\"tenbo_tienganh\\\">Ministry of Agriculture and Rural Development</a>\",\"isVisible\":1,\"parentId\":null,\"level\":1,\"tabPath\":\"\",\"isDeleted\":0,\"menuType\":0,\"children\":[{\"tabId\":171,\"tabOrder\":1,\"tabName\":\"Cấp Giấy chứng nhận kiểm dịch động vật, sản phẩm động vật trên cạn xuất khẩu\",\"isVisible\":1,\"parentId\":162,\"level\":2,\"tabPath\":\"/mard/01/home\",\"isDeleted\":0,\"menuType\":0,\"children\":null},{\"tabId\":176,\"tabOrder\":6,\"tabName\":\"Đăng ký kiểm dịch động vật, sản phẩm động vật thủy sản nhập khẩu\",\"isVisible\":1,\"parentId\":162,\"level\":2,\"tabPath\":\"/mard/06/home\",\"isDeleted\":0,\"menuType\":0,\"children\":null},{\"tabId\":177,\"tabOrder\":7,\"tabName\":\"Cấp giấy chứng nhận kiểm dịch động vật, sản phẩm động vật thủy sản nhập khẩu\",\"isVisible\":1,\"parentId\":162,\"level\":2,\"tabPath\":\"/mard/07/home\",\"isDeleted\":0,\"menuType\":0,\"children\":null},{\"tabId\":178,\"tabOrder\":8,\"tabName\":\"Đăng ký kiểm dịch động vật, sản phẩm động vật trên cạn nhập khẩu; đăng ký kiểm tra xác nhận chất lượng thức ăn chăn nuôi, thức ăn thủy sản có nguồn gốc động vật nhập khẩu\",\"isVisible\":1,\"parentId\":162,\"level\":2,\"tabPath\":\"/mard/08/home\",\"isDeleted\":0,\"menuType\":0,\"children\":null},{\"tabId\":179,\"tabOrder\":9,\"tabName\":\"Cấp Giấy chứng nhận kiểm dịch động vật, sản phẩm động vật trên cạn nhập khẩu; kiểm tra xác nhận chất lượng thức ăn chăn nuôi, thức ăn thủy sản có nguồn gốc động vật nhập khẩu.\",\"isVisible\":1,\"parentId\":162,\"level\":2,\"tabPath\":\"/mard/09/home\",\"isDeleted\":0,\"menuType\":0,\"children\":null}]}],\"companyName\":\":companyName\",\"companyAddress\":\"Việt Nam\",\"companyEmail\":\":companyEmail\",\"companyFax\":\":companyFax\",\"companyPhoneNumber\":\":companyPhoneNumber\",\"representerName\":\":representerName\",\"representerPhone\":\":representerPhone\",\"representerMobile\":\":representerMobile\",\"representerEmail\":\":representerEmail\"}";
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private int id;

    String representerName;
    String representerPhone;
    String representerMobile;
    String representerEmail;
    String companyName;
    String companyPhone;
    String companyEmail;
    String companyFax;
    String taxCode;
    String userName;

    @Column(name="user_password")
    String password;

    public String toNSWUserJson(){
        return BASE_JSON
                .replace(":representerName", representerName)
                .replace(":representerPhone", representerPhone)
                .replace(":representerMobile", representerMobile)
                .replace(":representerEmail", representerEmail)
                .replace(":companyName", companyName)
                .replace(":companyPhoneNumber", companyPhone)
                .replace(":companyEmail", companyEmail)
                .replace(":companyFax", companyFax)
                .replace(":taxCode", taxCode)
                .replace(":userName", userName)
                .replace(":password", password)
                ;
    }
}
