package com.nsw.parser.model;

import lombok.Data;

import java.util.ArrayList;
import java.util.List;

@Data
public class XLSElement {
    private String tag;
    private String property;
    private int level;
    private String description;
    private boolean repetition;
    private boolean required;
    private String dataType;
    private int length;
    private int fraction;

    private boolean wrapper;

    private List<XLSElement> attribute;

    public XLSElement() {
        attribute = new ArrayList<>();
    }

    public String getTag() {
        return (tag == null) ? "" : tag;
    }

    public String getProperty() {
        return (property == null) ? "" : property;
    }
}
