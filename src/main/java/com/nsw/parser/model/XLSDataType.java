package com.nsw.parser.model;

import lombok.Data;

@Data
public class XLSDataType {
    private String dataType;

    private int length;

    private int fractionDigits;
}
