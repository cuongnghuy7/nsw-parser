package com.nsw.parser.utils;

public class Constants {
    private static final String USER_DIRECTORY = System.getProperty("user.dir");
    public static final String TMP_FOLDER = USER_DIRECTORY + "/tmp/";

    public static final String XSD_EXTENSIONS = ".xsd";

    //yyyy-MM-dd hh:mm:ss.zzz
    public static final String DATE_PATTERN = "(\\d{4}-(0[1-9]|1[0-2])-(0[1-9]|[12]\\d|3[01]) ([01]\\d|2[0-3])(:[0-5]\\d){2}(\\.\\d{3})?)?";
}
