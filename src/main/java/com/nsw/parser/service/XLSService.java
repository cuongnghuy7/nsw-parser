package com.nsw.parser.service;

import com.nsw.parser.model.XLSDataType;
import com.nsw.parser.model.XLSElement;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.springframework.stereotype.Service;
import org.springframework.util.StringUtils;

import java.util.ArrayDeque;
import java.util.Deque;
import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicBoolean;

@Service
public class XLSService {
    private static final Log log = LogFactory.getLog(XLSService.class);

    private static final String LEGEND_TAG = "Tag";
    private static final String LEGEND_PROPERTY = "Property";
    private static final String LEGEND_DESCRIPTION = "Description";
    private static final String LEGEND_REQUIRED = "Required";
    private static final String LEGEND_TYPE = "Type";
    private static final String LEGEND_LEVEL = "Level";
    private static final String LEGEND_REPEAT = "Repeat";


    private DataFormatter formatter;

    public XLSElement parseSheet(Sheet sheet) {
        Deque<XLSElement> elementStack = new ArrayDeque<>();
        Map<String, Integer> legend = new HashMap<>();
        AtomicBoolean afterLegendRow = new AtomicBoolean(false);
        sheet.rowIterator().forEachRemaining(row -> processRow(sheet, elementStack, legend, afterLegendRow, row));
        if (elementStack.isEmpty()) {
            log.error("Sheet generated empty stack: " + sheet.getSheetName());
            throw new IllegalArgumentException("Empty stack");
        } else {
            elementStack.pop();
            if (elementStack.size() != 1) {
                log.error("Sheet generated malformed stack: " + sheet.getSheetName());
                while (!elementStack.isEmpty()) {
                    XLSElement element = elementStack.pop();
                    log.info("Stack: " + element.getTag() + "|" + element.getProperty());
                }
                throw new IllegalArgumentException("Malformed stack @" + sheet.getSheetName());
            }
            return elementStack.pop();
        }

    }

    private void processRow(Sheet sheet, Deque<XLSElement> elementStack, Map<String, Integer> legend, AtomicBoolean afterLegendRow, Row row) {
        try {
            //Init legend
            if (!afterLegendRow.get() && "STT".equalsIgnoreCase(getFormatter().formatCellValue(row.getCell(0)))) {
                parseLegend(legend, row);
                afterLegendRow.set(true);
            } else if (afterLegendRow.get()) {
                XLSElement element = new XLSElement();
                if (row.getCell(legend.get(LEGEND_TAG)) != null
                        && !StringUtils.isEmpty(row.getCell(legend.get(LEGEND_TAG)).getStringCellValue())) {
                    element.setTag(row.getCell(legend.get(LEGEND_TAG)).getStringCellValue().trim());
                }
                if (row.getCell(legend.get(LEGEND_PROPERTY)) != null
                        && !StringUtils.isEmpty(row.getCell(legend.get(LEGEND_PROPERTY)).getStringCellValue())) {
                    element.setProperty(row.getCell(legend.get(LEGEND_PROPERTY)).getStringCellValue().trim());
                }
                //EMPTY ROW
                if (StringUtils.isEmpty(element.getTag())
                        && StringUtils.isEmpty(element.getProperty())) {
                    StringBuilder sb = new StringBuilder();
                    row.cellIterator().forEachRemaining(cell -> sb.append(getFormatter().formatCellValue(cell)).append('|'));
                    log.info("Empty row : " + sheet.getSheetName() + " : " + row.getRowNum() + sb.toString());
                    afterLegendRow.set(false);
                } else {
                    if (!StringUtils.isEmpty(element.getTag()) && !StringUtils.isEmpty(element.getProperty())) {
                        log.warn(String.format("Multiple TAG/PROPERTY : %s:%s / %s|%s", sheet.getSheetName(), row.getRowNum(), element.getTag(), element.getProperty()));
                    }

                    if (element.getProperty().trim().contains(" ") || element.getTag().trim().contains(" ")) {
                        throw new Exception("Field must not contains space character");
                    }
                    element.setDescription(row.getCell(legend.get(LEGEND_DESCRIPTION)).getStringCellValue());
//                        element.setLevel((int) row.getCell(legend.get("Level")).getNumericCellValue());
//                        element.setRepetition(!StringUtils.isEmpty(formatter.formatCellValue(row.getCell(5))));
                    element.setRequired(!StringUtils.isEmpty(getFormatter().formatCellValue(row.getCell(legend.get(LEGEND_REQUIRED)))));
                    String rawType = row.getCell(legend.get(LEGEND_TYPE)).getStringCellValue().toLowerCase();

                    XLSDataType dataType = extractDataType(rawType);
                    element.setDataType(dataType.getDataType());
                    element.setFraction(dataType.getFractionDigits());
                    element.setLength(dataType.getLength());

                    String rawTag = row.getCell(legend.get(LEGEND_TAG)).getStringCellValue().toLowerCase().trim();
                    XSSFCellStyle cellStyle;
                    if (StringUtils.isEmpty(rawTag)) {
                        cellStyle = (XSSFCellStyle) row.getCell(legend.get(LEGEND_PROPERTY)).getCellStyle();
                    } else {
                        cellStyle = (XSSFCellStyle) row.getCell(legend.get(LEGEND_TAG)).getCellStyle();
                    }
                    if (StringUtils.isEmpty(rawType) && !cellStyle.getFont().getBold()) {
                        log.warn("Raw data type null @" + sheet.getSheetName() + ":" + row.getRowNum());
                        log.warn(element);
                    }

                    stackProcess(elementStack, element, cellStyle);
                }
            }
        } catch (Exception ex) {
            log.error("Sheet generated Exception : " + sheet.getSheetName() + ":" + row.getRowNum());
            log.error(ex);
//            throw new IllegalArgumentException(ex);
        }
    }

    private void stackProcess(Deque<XLSElement> elementStack, XLSElement element, XSSFCellStyle cellStyle) throws Exception {
        if (!StringUtils.isEmpty(element.getTag())) {
            elementStack.push(element);
        } else if (element.getProperty().equals(elementStack.peek().getProperty())) {
            XLSElement topElement = elementStack.pop();
            if (elementStack.isEmpty()) {
                elementStack.push(topElement);
            }
        } else {
            XLSElement topElement = elementStack.pop();
            if (!cellStyle.getFont().getBold()
                    && topElement.getAttribute().stream().map(xls -> xls.getTag() + xls.getProperty())
                    .anyMatch(field -> field.equals(element.getTag() + element.getProperty()))) {
                throw new Exception("Duplicate field " + element.getTag() + element.getProperty());
            }
            topElement.getAttribute().add(element);
            elementStack.push(topElement);
            if (cellStyle.getFont().getBold()) {
                elementStack.push(element);
            }
        }
    }

    private XLSDataType extractDataType(String rawType) {
        XLSDataType type = new XLSDataType();
        if (StringUtils.isEmpty(rawType) || rawType.startsWith("a")) {
            rawType = clearPrefix(rawType);
            type.setDataType("String");
            type.setLength(StringUtils.isEmpty(rawType) ? 250 : Integer.parseInt(rawType));
        } else if (rawType.startsWith("b")) {
            type.setDataType("Base64");
        } else if (rawType.startsWith("n")) {
            rawType = clearPrefix(rawType);
            while (rawType.startsWith(".")) {
                rawType = rawType.substring(1);
            }
            String length = rawType;
            if (length.split(",").length > 1) {
                type.setDataType("Double");
                type.setLength(Integer.parseInt(length.split(",")[0]));
                type.setFractionDigits(Integer.parseInt(length.split(",")[1]));
                if (type.getLength() > 20) {
                    throw new IllegalArgumentException("Malformed data length for number: " + type.getLength());
                }
            } else {
                type.setLength(Integer.parseInt(length));
                if (type.getLength() < 5) {
                    type.setDataType("Integer");
                } else if (type.getLength() < 21) {
                    type.setDataType("Long");
                } else {
                    throw new IllegalArgumentException("Malformed data length for number: " + type.getLength());
                }
            }
        } else if (rawType.startsWith("d")) {
            //should be date
            type.setDataType(StringUtils.capitalize(rawType));
        } else {
            log.warn("Unknown datatype: " + rawType);
            throw new IllegalArgumentException("Unknown datatype: " + rawType);
        }
        return type;
    }

    private String clearPrefix(String rawType) {
        while (rawType.startsWith(".") || rawType.startsWith("a") || rawType.startsWith("n")) {
            rawType = rawType.substring(1);
        }
        return rawType;
    }

    private void parseLegend(Map<String, Integer> legend, Row row) {
        row.cellIterator().forEachRemaining(cell -> {
            addLegend(legend, cell, LEGEND_TAG);
            addLegend(legend, cell, LEGEND_PROPERTY);
            addLegend(legend, cell, LEGEND_LEVEL);
            addLegend(legend, cell, LEGEND_DESCRIPTION);
            addLegend(legend, cell, LEGEND_REQUIRED);
            addLegend(legend, cell, LEGEND_TYPE);
            addLegend(legend, cell, LEGEND_REPEAT);
        });
    }

    private void addLegend(Map<String, Integer> legend, Cell cell, String legendKey) {
        if (cell.getStringCellValue().contains(legendKey)) {
            legend.put(legendKey, cell.getColumnIndex());
        }
    }

    private DataFormatter getFormatter() {
        if (formatter == null) {
            formatter = new DataFormatter();
        }
        return formatter;
    }
}
