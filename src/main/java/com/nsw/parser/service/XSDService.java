package com.nsw.parser.service;

import com.nsw.parser.ParserApplication;
import com.nsw.parser.model.XLSElement;
import com.nsw.parser.utils.Constants;
import com.nsw.parser.utils.ZipCompress;
import freemarker.template.*;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Service;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;
import org.w3c.dom.Attr;
import org.w3c.dom.Document;
import org.w3c.dom.Element;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import java.io.*;
import java.util.*;

@Service
public class XSDService {
    public static final String VALUE = "value";
    private static final Log log = LogFactory.getLog(XSDService.class);
    private static final String BASE_TEMPLATE = "base.ftl";
    private static final String BASE_TEMPLATE_RESOURCE_DIR = "templates/freemarker";

    public void generateXSDSchema(XLSElement rootXls, String sheetName, String filePrefix) {
        try {
            DocumentBuilderFactory documentFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder documentBuilder = documentFactory.newDocumentBuilder();
            Document document = documentBuilder.newDocument();
            // root element
            Element rootWrapper = createElement(rootXls, document, false);
            document.appendChild(rootWrapper);

            log.trace("Created root element!");

            //transform the DOM Object to an XML File
            TransformerFactory transformerFactory = TransformerFactory.newInstance();
            Transformer transformer = transformerFactory.newTransformer();
            transformer.setOutputProperty("omit-xml-declaration", "yes");

            //Write output to byte array
            DOMSource domSource = new DOMSource(document);
            ByteArrayOutputStream arrayOutputStream = new ByteArrayOutputStream();
            StreamResult streamResult = new StreamResult(arrayOutputStream);
            transformer.transform(domSource, streamResult);

            Map<String, Object> input = new HashMap<>();
            input.put("root_content", new String(arrayOutputStream.toByteArray()));

            writeToFile(generateOutputFile(sheetName, filePrefix), input);
        } catch (ParserConfigurationException | TransformerException e) {
            log.error("Error while while transforming JAXB Object");
            log.error(e);
        }
    }

    private void writeToFile(List<File> outputFile, Map<String, Object> input) {
        try {
            Template template = getFreeMarkerConfiguration().getTemplate(BASE_TEMPLATE);
            Writer consoleWriter = new OutputStreamWriter(new ByteArrayOutputStream());
            template.process(input, consoleWriter);
            for (File file : outputFile) {
                Writer fileWriter = new FileWriter(file);
                template.process(input, fileWriter);
                fileWriter.flush();
                fileWriter.close();
            }
        } catch (IOException e) {
            log.error("Error while writing output File!");
            log.error(e);
        } catch (TemplateException e) {
            log.error("Error while processing template!");
            log.error(e);
        } catch (IllegalArgumentException iae) {
            //ERROR ON CONFIGURATION
        }

    }

    private Configuration getFreeMarkerConfiguration() {
        Configuration cfg = new Configuration(new Version(2, 3, 23));
        cfg.setClassForTemplateLoading(ParserApplication.class, "/templates/freemarker/");
        cfg.setIncompatibleImprovements(new Version(2, 3, 23));
        cfg.setDefaultEncoding("UTF-8");
        cfg.setLocale(Locale.US);
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        return cfg;
    }

    private List<File> generateOutputFile(String sheetName, String filePrefix) {
        File rootFile = new File(Constants.TMP_FOLDER + filePrefix);
        if (!rootFile.exists()) {
            rootFile.mkdirs();
        }
        List<File> outputFile = new ArrayList<>();
        String functions = sheetName.substring(3, sheetName.indexOf(')'));
        for (String func : functions.split(",")) {
            func = func.trim();
            String fileName = filePrefix + sheetName.substring(0, 2) + func + Constants.XSD_EXTENSIONS;
            outputFile.add(new File(rootFile, fileName));
        }
        return outputFile;
    }

    public void deleteFolder(File rootFile) {
        if (rootFile.exists()) {
            Arrays.stream(rootFile.listFiles()).forEach(file -> {
                if (file.isDirectory()) {
                    deleteFolder(file);
                } else {
                    file.delete();
                }
            });
        }
    }

    private Element createElement(XLSElement xls, Document document, boolean isRepeat) {
        Element element = document.createElement("xs:element");

        String elementName = xls.getTag();
        if (StringUtils.isEmpty(elementName)) {
            elementName = xls.getProperty();
        }
        element.setAttributeNode(createAttr("name", elementName, document));

        if (!xls.isRequired()) {
            element.setAttributeNode(createAttr("minOccurs", "0", document));
            element.setAttributeNode(createAttr("nillable", "true", document));
        }
        if(isRepeat){
            element.setAttributeNode(createAttr("minOccurs", "0", document));
            element.setAttributeNode(createAttr("nillable", "true", document));
            element.setAttributeNode(createAttr("maxOccurs", "unbounded", document));
        }
        if (CollectionUtils.isEmpty(xls.getAttribute())) {
            Element simpleType = createSimpleElement(document, "xs:simpleType");
            Element restriction = createSimpleElement(document, "xs:restriction");

            if (xls.getDataType().equals("String")) {
                createStringRestriction(xls, document, restriction);
            } else if (xls.getDataType().equals("Base64")) {
                createBase64Restriction(xls, document, restriction);
            } else if (xls.getDataType().equals("Double")) {
                createDoubleRestriction(xls, document, restriction);
            } else if (xls.getDataType().equals("Long") || xls.getDataType().equals("Integer")) {
                createLongRestriction(xls, document, restriction);
            } else {
                createDateRestriction(document, restriction);
            }

            simpleType.appendChild(restriction);
            element.appendChild(simpleType);
        } else {
            boolean repetition = xls.getAttribute().size() == 1;
            Element childComplex = createComplexType(document, repetition);
            element.appendChild(childComplex);
            for (XLSElement child : xls.getAttribute()) {
                childComplex.getFirstChild().appendChild(createElement(child, document, repetition));
            }
        }
        return element;
    }

    private void createDateRestriction(Document document, Element restriction) {
        restriction.setAttributeNode(createAttr("base", "xs:string", document));
        Element pattern = createSimpleElement(document, "xs:pattern");
        pattern.setAttributeNode(createAttr(VALUE, Constants.DATE_PATTERN, document));
        restriction.appendChild(pattern);
    }

    private void createLongRestriction(XLSElement xls, Document document, Element restriction) {
        if (xls.getDataType().equals("Long")) {
            restriction.setAttributeNode(createAttr("base", "xs:long", document));
        } else {
            restriction.setAttributeNode(createAttr("base", "xs:short", document));
        }
        Element maxExclusive = createSimpleElement(document, "xs:maxExclusive");
        Element minInclusive = createSimpleElement(document, "xs:minInclusive");
        maxExclusive.setAttributeNode(createAttr(VALUE, String.format("%.0f", Math.pow(10, xls.getLength())), document));
        minInclusive.setAttributeNode(createAttr(VALUE, "0", document));
        restriction.appendChild(maxExclusive);
        restriction.appendChild(minInclusive);
    }

    private void createDoubleRestriction(XLSElement xls, Document document, Element restriction) {
        restriction.setAttributeNode(createAttr("base", "xs:decimal", document));
        Element maxExclusive = createSimpleElement(document, "xs:maxExclusive");
        Element fractionDigits = createSimpleElement(document, "xs:fractionDigits");
        maxExclusive.setAttributeNode(createAttr(VALUE, String.format("%.0f", Math.pow(10, xls.getLength())), document));
        fractionDigits.setAttributeNode(createAttr(VALUE, xls.getFraction() + "", document));
        restriction.appendChild(maxExclusive);
        restriction.appendChild(fractionDigits);
    }

    private void createStringRestriction(XLSElement xls, Document document, Element restriction) {
        if (xls.isRequired()) {
            restriction.setAttributeNode(createAttr("base", "NonEmptyString", document));
        } else {
            restriction.setAttributeNode(createAttr("base", "xs:string", document));
        }
        Element maxLength = createSimpleElement(document, "xs:maxLength");
        maxLength.setAttributeNode(createAttr(VALUE, xls.getLength() + "", document));
        restriction.appendChild(maxLength);
    }

    private void createBase64Restriction(XLSElement xls, Document document, Element restriction) {
        if (xls.isRequired()) {
            restriction.setAttributeNode(createAttr("base", "NonEmptyString", document));
        } else {
            restriction.setAttributeNode(createAttr("base", "xs:string", document));
        }
    }

    private Attr createAttr(String name, String elementName, Document document) {
        Attr attr = document.createAttribute(name);
        attr.setValue(elementName);
        return attr;
    }

    private Element createComplexType(Document document, boolean repetition) {
        Element complexType = document.createElement("xs:complexType");
        if (repetition) {
            Element sequence = document.createElement("xs:sequence");
            complexType.appendChild(sequence);
        } else {
            Element sequence = document.createElement("xs:all");
            complexType.appendChild(sequence);
        }
        return complexType;
    }

    private Element createSimpleElement(Document document, String tagName) {
        return document.createElement(tagName);
    }

    public File getZipFile(String prefix) {
        File rootFile = new File(Constants.TMP_FOLDER + prefix + ".zip");
        rootFile.delete();
        return ZipCompress.compress(Constants.TMP_FOLDER + prefix);
    }
}
