package com.nsw.parser.controller;

import com.nsw.parser.model.XLSElement;
import com.nsw.parser.service.XLSService;
import com.nsw.parser.service.XSDService;
import com.nsw.parser.utils.Constants;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.core.io.Resource;
import org.springframework.core.io.UrlResource;
import org.springframework.http.HttpHeaders;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;


@Controller
public class FileController {
    private static final Log log = LogFactory.getLog(FileController.class);


    @Autowired
    XLSService xlsService;

    @Autowired
    XSDService xsdService;

    @GetMapping(value = {"", "home", "index"})
    public String homePage() {
        return "uploadXLS";
    }

    @PostMapping("uploadXLS")
    public ResponseEntity<Resource> uploadXLS(@RequestParam MultipartFile file, @RequestParam String prefix) throws IOException {
        log.info(file.getOriginalFilename());
        ByteArrayInputStream byteArrayInputStream = new ByteArrayInputStream(file.getBytes());
        log.info("Uploaded file complete");
        long currTime = System.currentTimeMillis();
        Workbook currentWorkbook = new XSSFWorkbook(byteArrayInputStream);
        log.info(String.format("Loaded %s in %.3fs", file.getOriginalFilename(), (System.currentTimeMillis() - currTime) / 1000f));
        File rootFile = new File(Constants.TMP_FOLDER + prefix);
        xsdService.deleteFolder(rootFile);
        currentWorkbook.sheetIterator().forEachRemaining(sheet -> {
            if (sheet.getSheetName().indexOf('(') == 2) {
                XLSElement root = xlsService.parseSheet(sheet);
                xsdService.generateXSDSchema(root, sheet.getSheetName(), prefix);
            }
        });
        File storedFile = xsdService.getZipFile(prefix);
        Resource resource = new UrlResource(storedFile.toURI());
        return ResponseEntity.ok()
                .contentType(MediaType.APPLICATION_OCTET_STREAM)
                .header(HttpHeaders.CONTENT_DISPOSITION,
                        "attachment; filename=\""
                                + prefix + ".zip"
                                + "\"")
                .body(resource);
    }


}
