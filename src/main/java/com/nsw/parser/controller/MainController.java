package com.nsw.parser.controller;

import com.nsw.parser.model.User;
import com.nsw.parser.repositories.UserRepository;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PostMapping;

@Controller
public class MainController {
    @Autowired
    UserRepository userRepository;

    @GetMapping("/user")
    public String getCreateView(Model model){
        User user = new User();
        model.addAttribute("user", user);
        return "createAccount";
    }

    @PostMapping("/create")
    public String createUser(@ModelAttribute("user") User user){
        User oldUser = userRepository.findByUserName(user.getUserName()).orElse(null);
        if(oldUser == null){
            userRepository.save(user);
        }
        else{
            int oldId = oldUser.getId();
            BeanUtils.copyProperties(user, oldUser);
            oldUser.setId(oldId);
            userRepository.save(oldUser);
        }
        return "redirect:user";
    }
}
