package com.nsw.parser.controller;

import com.nsw.parser.model.User;
import com.nsw.parser.repositories.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import javax.xml.ws.Response;

@RestController
public class UserDetailController {

    @Autowired
    UserRepository userRepository;

    @GetMapping("/users")
    public String getUserByUsername(@RequestParam String username){
        User user = userRepository.findByUserName(username).orElse(null);
        if(user != null){
            return user.toNSWUserJson();
        }
        else {
            return "";
        }
    }

    @PostMapping("/doi-mat-khau")
    public ResponseEntity<String> changePass(@RequestParam String username, @RequestParam String newPass){
        User user = userRepository.findByUserName(username).orElse(null);
        if(user != null){
            user.setPassword(newPass);
            return ResponseEntity.ok(userRepository.save(user).toNSWUserJson());
        }
        else {
            return ResponseEntity.ok("User not found!");
        }
    }
}
